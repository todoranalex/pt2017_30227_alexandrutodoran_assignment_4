import java.io.Serializable;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

/**
 * Created by Alex on 4/23/2017.
 */
public class Person implements Observer,Serializable {

    private ArrayList<Account> accounts ;
    private String name;



    public Person(String name){

        this.name = name;
        accounts = new ArrayList<>();
    }

    public void setName(String name){
        this.name = name;
    }

    public String getName(){
        return name;
    }

    public void setAccounts(ArrayList<Account>accounts){
        this.accounts = accounts;
    }

    public ArrayList<Account> getAccounts(){
        return accounts;
    }

    public String toString(){
        return name;
    }

    public void update(Observable obs, Object obj){

        System.out.println(" --PERSON NOTIFIED-- "+ name +": ammount changed to: " + obj);
    }
}
