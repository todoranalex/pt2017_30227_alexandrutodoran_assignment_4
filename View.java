import javafx.scene.input.MouseEvent;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;


public class View extends JFrame {


    private JTable table;
    private JPanel mainPanel = new JPanel();
    private JPanel panel = new JPanel();
    private JButton getPerson = new JButton("Select");
    private JButton makeWithdraw = new JButton("Withdraw from SpendingAccount");
    private JButton addAmount = new JButton("Add amount to SavingAccount");
    private JTextField amount = new JTextField("insert amount");
    private JButton showPersons = new JButton("Show Persons");
    private JButton showAccounts = new JButton("Show Accounts");
    private JButton makeWithdrawSaving = new JButton("Withdraw from SavingAccount");
    private JButton addAmounttoSpending = new JButton("Add amount to SpendingAccount");
    private JButton addInterest = new JButton("Add interest to SavingAccount");
    private JButton addPerson = new JButton("Add Person");
    private JButton addAccount = new JButton("Add Account");


    private void tableSetup(JTable table) {
        this.table = table;
        setLayout(new FlowLayout());

        table.setPreferredScrollableViewportSize(new Dimension(300, 150));
        table.setFillsViewportHeight(true);

        JScrollPane scrollPane = new JScrollPane(table);
        add(scrollPane);


        setSize(600, 400);
        setVisible(true);

    }

    private void addComponents1(){
        showPersons.setBounds(100,200,150,40);
        showAccounts.setBounds(245,200,150,40);
        makeWithdraw.setBounds(100,250,230,40);
        makeWithdrawSaving.setBounds(330,250,230,40);
        addAmount.setBounds(100,300,230,40);
        amount.setBounds(500,400,150,40);
        addAmounttoSpending.setBounds(330,300,230,40);
        addInterest.setBounds(100,350,230,40);
        addPerson.setBounds(100,150,150,40);
        addAccount.setBounds(245,150,150,40);

        panel.add(getPerson);
        panel.add(makeWithdraw);
        panel.add(addAmount);
        panel.add(amount);
        panel.add(addAmounttoSpending);
        panel.add(showPersons);
        panel.add(showAccounts);
        panel.add(makeWithdrawSaving);
        panel.add(addInterest);
        panel.add(addPerson);
        panel.add(addAccount);


    }

    public View() {

        add(panel);
        panel.setLayout(null);
        addComponents1();
        jFrameSetup();

    }



    private void addComponents() {

        addAmounttoSpending.setBounds(0,500,150,40);
        mainPanel.add(getPerson);

    }

    public View(JTable table) {

        tableSetup(table);
        add(mainPanel);

       // mainPanel.setLayout(null);
        addComponents();
        jFrameSetup1();

    }
    private void jFrameSetup() {
        setSize(700, 500);
        setResizable(false);
        setVisible(true);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        // setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
    }

    private void jFrameSetup1() {
        setSize(500, 300);
        setResizable(false);
        mainPanel.setVisible(true);
       // setDefaultCloseOperation(EXIT_ON_CLOSE);
         setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
    }

    public void attachButton1(ActionListener a) {
        showPersons.addActionListener(a);
    }
    public void attachButton2(ActionListener a) {
        showAccounts.addActionListener(a);
    }
    public void attachButton3(ActionListener a) {
        getPerson.addActionListener(a);
    }
    public void attachButton4(ActionListener a) {
        makeWithdraw.addActionListener(a);
    }
    public void attachButton5(ActionListener a) {
        addAmount.addActionListener(a);
    }
    public void attachButton6(ActionListener a) {
        addAmounttoSpending.addActionListener(a);
    }
    public void attachButton7(ActionListener a) {
        makeWithdrawSaving.addActionListener(a);
    }
    public void attachButton8(ActionListener a) {
        addInterest.addActionListener(a);
    }
    public void attachButton9(ActionListener a) {
        addPerson.addActionListener(a);
    }

    public float getAmount(){
        return Float.parseFloat(amount.getText());

    }

}

