import java.io.Serializable;
import java.util.Observable;

/**
 * Created by Alex on 4/23/2017.
 */
public abstract class Account extends Observable implements Serializable {

    private String name;
    private double currentAmount;



    public Account(){};

    public Account(String name,double currentAmount){
        this.name = name;
        this.currentAmount = currentAmount;
    }

    public String getName(){
        return name;
    }

    public double getCurrentAmount() {
        return currentAmount;
    }

    public void setCurrentAmount(double currentAmount) {
        this.currentAmount = currentAmount;
    }


    public void makeWithdrawal( int amount){

        assert amount >=0 : "ERROR! amount < 0";
        System.out.println(amount);
        this.currentAmount =  currentAmount - amount;

        assert currentAmount >=0 : "ERROR! Not enough amount in account to withdraw";
        setChanged();
        notifyObservers(currentAmount);

    }

    public void addAmount(double amount){

        assert amount >=0 : "ERROR! amount < 0";

        this.currentAmount = currentAmount + amount;
        setChanged();
        notifyObservers(currentAmount);

    }
    public void addInterest(){
    };

}
