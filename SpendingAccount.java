import java.io.Serializable;

/**
 * Created by Alex on 4/23/2017.
 */
public class SpendingAccount extends Account implements Serializable {

    public SpendingAccount(){
        super();
    }

    public SpendingAccount(String name, double currentAmount){

        super(name,currentAmount);
    }

    public String toString(){
        return "--SpendingAccount-- "+ getName() +" currentAmount: "+getCurrentAmount();
    }

}
