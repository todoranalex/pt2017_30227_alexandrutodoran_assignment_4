import javafx.scene.input.MouseEvent;

import javax.swing.*;
import javax.swing.table.TableModel;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.Hashtable;

/**
 * Created by Alex on 5/3/2017.
 */


public class Controller {

    private View view;
    private View personView;
    private View accountView;
    private Hashtable<Person, ArrayList<Account>> hashTable;
    private Bank bank;
    private float amount;
    private JTable table1;
    private JTable table2;
    private String personName;
    private String accountName;

    public Controller(Hashtable<Person, ArrayList<Account>> hashTable) {
        view = new View();
        this.hashTable = hashTable;
        this.bank = new Bank();
        view.attachButton1(new ShowPersons());
        view.attachButton2(new showAccounts());
        view.attachButton4(new makeWithdrawFromSpending());
        view.attachButton5(new addAmounttoSaving());
        view.attachButton6(new addAmountToSpending());
        view.attachButton7(new makeWithdrawFromSaving());
        view.attachButton8(new addInterest());
        view.attachButton9(new addPerson());


    }


    class ShowPersons implements ActionListener {

        public void actionPerformed(ActionEvent e) {

            table1 = new JTable(bank.toTableModelPerson(hashTable));
            personView = new View(table1);
            personView.attachButton3(new getPerson());

        }

    }

    class showAccounts implements ActionListener {

        public void actionPerformed(ActionEvent e) {

            table2 = new JTable(bank.toTableModelAccount(hashTable));

            accountView = new View(table2);


        }

    }

    class getPerson implements ActionListener {

        public void actionPerformed(ActionEvent e) {

            int personColumn = table1.getSelectedColumn();
            int personRow = table1.getSelectedRow();
            personName = table1.getModel().getValueAt(personRow, personColumn).toString();
            System.out.println(personName);

        }
    }

    class makeWithdrawFromSaving implements ActionListener {

        public void actionPerformed(ActionEvent e) {


            Person personSelected = new Person(personName);
            for (Person person : hashTable.keySet()) {
                if (person.getName() == personSelected.getName()) {
                    Account account = hashTable.get(person).get(0);
                    amount = view.getAmount();
                    account.addObserver(person);
                    account.makeWithdrawal((int) amount);
                    table2.repaint();
                    System.out.println(account.getCurrentAmount());
                }

            }

        }
    }

    class makeWithdrawFromSpending implements ActionListener {

        public void actionPerformed(ActionEvent e) {


            Person personSelected = new Person(personName);
            for (Person person : hashTable.keySet()) {
                if (person.getName() == personSelected.getName()) {
                    Account account = hashTable.get(person).get(1);
                    amount = view.getAmount();
                    account.addObserver(person);
                    account.makeWithdrawal((int) amount);
                    table2.repaint();
                    System.out.println(account.getCurrentAmount());
                }

            }

        }
    }

    class addAmounttoSaving implements ActionListener {

        public void actionPerformed(ActionEvent e) {


            Person personSelected = new Person(personName);
            for (Person person : hashTable.keySet()) {
                if (person.getName() == personSelected.getName()) {
                    Account account = hashTable.get(person).get(0);
                    amount = view.getAmount();
                    account.addObserver(person);
                    account.addAmount((int) amount);
                    table2.repaint();
                    System.out.println(account.getCurrentAmount());
                }

            }

        }
    }

    class addAmountToSpending implements ActionListener {

        public void actionPerformed(ActionEvent e) {


            Person personSelected = new Person(personName);
            for (Person person : hashTable.keySet()) {
                if (person.getName() == personSelected.getName()) {
                    Account account = hashTable.get(person).get(1);
                    amount = view.getAmount();
                    account.addObserver(person);
                    account.addAmount((int) amount);
                    table2.repaint();
                    System.out.println(account.getCurrentAmount());
                }

            }

        }
    }

    class addInterest implements ActionListener {

        public void actionPerformed(ActionEvent e) {


            Person personSelected = new Person(personName);
            for (Person person : hashTable.keySet()) {
                if (person.getName() == personSelected.getName()) {
                    Account account = hashTable.get(person).get(0);
                    account.addObserver(person);
                    account.addInterest();
                    table2.repaint();
                    System.out.println(account.getCurrentAmount());

                }

            }

        }
    }

    class addPerson implements ActionListener{

        public void actionPerformed(ActionEvent e){

            Person person = new Person("NewPerson");
            Account acc1 = new SavingAccount();
            Account acc2 = new SpendingAccount();
            ArrayList<Account> list = new ArrayList<>();
            //bank.addPerson(person);
            list.add(acc1);
            list.add(acc2);
            hashTable.put(person,list);
           table1.repaint();
           table2.repaint();
            System.out.println("pressed");
        }

    }





}
