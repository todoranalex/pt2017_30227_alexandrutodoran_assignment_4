import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by Alex on 5/8/2017.
 */
class BankTest {
    @org.junit.jupiter.api.Test
    void makeWithdrawal() {

        Bank bank = new Bank();
        Person person = new Person("p1");
        Account account = new SpendingAccount("acc1",500);
        bank.addPerson(person);
        bank.addAccount(person,account);
        bank.makeWithdrawal(person,account,300);
        assertEquals(account.getCurrentAmount(),200);

    }

//    @org.junit.jupiter.api.Test
//    void addAmount() {
//    }

}