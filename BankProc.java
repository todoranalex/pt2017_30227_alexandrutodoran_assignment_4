import java.util.ArrayList;
import java.util.Hashtable;

/**
 * Created by Alex on 5/2/2017.
 */
public interface BankProc {

/*
@pre
 - check if the persons already exists
 */
    public void addPerson(Person person);
    public void addAccount(Person person,Account account);

    /*
    @pre
        - check if amount inserted is greater or equal with 0 before making a withdrawal;
     */

    public void makeWithdrawal(Person person, Account account, int amount);

    /*
     @post
        - check if the remaining amount is greater that 0 after making a withdrawal;
     */

    /*
    @pre
        - check if amount inserted is greater or equal with 0 before adding amount into account;
     */
    public void addAmount(Person person, Account account, int amount);
    public void addInterest(Person person, Account account);








}
