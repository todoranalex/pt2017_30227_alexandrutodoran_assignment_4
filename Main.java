import javax.swing.*;
import java.io.*;
import java.util.ArrayList;
import java.util.Hashtable;

/**
 * Created by Alex on 5/1/2017.
 */
public class Main {

    public static void main(String[] args){

        Bank bank = new Bank();

//        Person person1 = new Person("Alex");
//        Account account1 = new SavingAccount("Acc1",200,4);
//        Account account2 = new SpendingAccount("Acc2",500);
//
//        Person person2 = new Person("Alin");
//        Account account3 = new SavingAccount("Acc3",300,5);
//        Account account4 = new SpendingAccount("Acc4",700);

        //Person person 3 = new Person("Adrian");

        for( int  i = 1; i <= 5; i++){

            Person person = new Person("Person"+i);
            Account account1 = new SavingAccount("Person"+i,0,4);
            Account account2 = new SpendingAccount("Person"+i,0);
            bank.addPerson(person);
            bank.addAccount(person,account1);
            bank.addAccount(person,account2);
        }

//
//        bank.addPerson(person1);
//        bank.addPerson(person2);
//
//        bank.addAccount(person1,account1);
//        bank.addAccount(person1,account2);
//
//        bank.addAccount(person2,account3);
//        bank.addAccount(person2,account4);
//        account1.addObserver(person1);
//        account2.addObserver(person1);
//        account3.addObserver(person2);
//        account4.addObserver(person2);
//
//        bank.show();
//
//        System.out.println("After withdrawal");
//
//        bank.makeWithdrawal(person2,account4,400);


        try{
            FileOutputStream fisier = new FileOutputStream("hashtable.fisier");
            ObjectOutputStream objs = new ObjectOutputStream(fisier);
            objs.writeObject(bank.getHashTable());
            objs.close();
            fisier.close();
            System.out.printf("Serialized Hashtable data is saved in hashtable.fisier");
        }catch(IOException ioe)
        {
            ioe.printStackTrace();
        }

        Hashtable<Person, ArrayList<Account>> hashTable;

        try
        {
            FileInputStream fis = new FileInputStream("hashtable.fisier");
            ObjectInputStream ois = new ObjectInputStream(fis);
            hashTable = (Hashtable) ois.readObject();
            ois.close();
            fis.close();
        }catch(IOException ioe)
        {
            ioe.printStackTrace();
            return;
        }catch(ClassNotFoundException c)
        {
            System.out.println("Class not found");
            c.printStackTrace();
            return;
        }

//        for (Person person : hashTable.keySet()) {
//
//            System.out.println(person.toString() + " has Accounts " + hashTable.get(person).toString());
//        }
//
//        System.out.println();
//
//
//
////        bank.show();
////
////
////
////
////        System.out.println("After addAmount");
////
////        bank.addAmount(person2,account4,300);
////        bank.show();
////
////        System.out.println("After 1 month");
////
////        bank.addInterest(person1,account1);
////
////        bank.show();
//
//
//
//        //
//        //JTable table2 = new JTable(bank.toTableModelAccount(bank.getHashTable()));
//
//        //View view1 = new View(table1);
//        //View view2 = new View(table2);
        Controller c = new Controller(hashTable);

    }



}
