import java.io.Serializable;

/**
 * Created by Alex on 4/23/2017.
 */
public class SavingAccount extends Account implements Serializable {


    private double annualInterestRate;
    private double lastAmountOfInterestEarned;

    public SavingAccount(){
        super();
    }

    public SavingAccount(String name, int currentAmount, int anuualInterestRate){
        super(name,currentAmount);
        this.annualInterestRate = anuualInterestRate;
        lastAmountOfInterestEarned = 0.0;
    }

    public void addInterest() {

        // Get the monthly interest rate.
       double monthlyInterestRate = annualInterestRate / 12;

        // Calculate the last amount of interest earned.
        lastAmountOfInterestEarned = monthlyInterestRate * getCurrentAmount();

        // Add the interest to the balance.

        setCurrentAmount(getCurrentAmount()+ lastAmountOfInterestEarned);

        setChanged();
        notifyObservers(getCurrentAmount());

    }

    public double getAnnualInterestRate() {
        return annualInterestRate;
    }

    public void setAnnualInterestRate(int annualInterestRate) {
        this.annualInterestRate = annualInterestRate;
    }

    public double getLastAmountOfInterestEarned() {
        return lastAmountOfInterestEarned;
    }

    public void setLastAmountOfInterestEarned(int lastAmountOfInterestEarned) {
        this.lastAmountOfInterestEarned = lastAmountOfInterestEarned;
    }

    public String toString(){

        return "--SavingAccount-- "+ getName() +" currentAmount: "+getCurrentAmount();


    }

}
