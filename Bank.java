import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Hashtable;

/**
 * Created by Alex on 4/23/2017.
 */
public class Bank implements Serializable,BankProc {

    private Hashtable<Person, ArrayList<Account>> table;


    public Bank() {

        table = new Hashtable<>();
    }

    public void show() {

        for (Person person : table.keySet()) {

            System.out.println(person.toString() + " has Accounts " + table.get(person).toString());
        }

        System.out.println();

    }

    public void addPerson(Person person) {

        table.put(person, person.getAccounts());
    }


    public void addAccount(Person person, Account account) {

        for (Person keyPerson : table.keySet()) {

            if (keyPerson == person) {
                ArrayList<Account> acc = table.get(keyPerson);
                acc.add(account);
                table.put(keyPerson, acc);
            }
        }
    }

    public void makeWithdrawal(Person person, Account account, int amount) {

        assert amount >0;

        for (Person keyPerson : table.keySet()) {
            if (keyPerson == person) {
                ArrayList<Account> accounts = table.get(keyPerson);
                for (Account searched : accounts) {
                    if (searched.getName() == account.getName()) {
                        searched.makeWithdrawal(amount);
                        assert searched.getCurrentAmount() >0;
                    }
                }
            }
        }

    }

    public void addAmount(Person person, Account account, int amount) {

        for (Person keyPerson : table.keySet()) {
            if (keyPerson == person) {
                ArrayList<Account> accounts = table.get(keyPerson);
                for (Account searched : accounts) {
                    if (searched.getName() == account.getName()) {
                        searched.addAmount(amount);
                    }
                }
            }
        }
    }

    public void addInterest(Person person, Account account) {

        for (Person keyPerson : table.keySet()) {
            if (keyPerson == person) {
                ArrayList<Account> accounts = table.get(keyPerson);
                for (Account searched : accounts) {
                    if (searched.getName() == account.getName()) {
                        searched.addInterest();
                    }
                }
            }
        }

    }

    public static TableModel toTableModelPerson(Hashtable<Person, ArrayList<Account>> table) {
        DefaultTableModel model = new DefaultTableModel(
                new Object[]{"Person"}, 0
        );
        for(Person keyPerson: table.keySet()) {

            model.addRow(new Object[]{keyPerson});

        }
        return model;
    }

    public static TableModel toTableModelAccount (Hashtable<Person,ArrayList<Account>> table) {
        DefaultTableModel model = new DefaultTableModel(
                new Object[]{"Account"}, 0
        );
        for (Person keyPerson : table.keySet()) {
            ArrayList<Account> accounts = table.get(keyPerson);
            for (Account searched : accounts) {
                model.addRow(new Object[]{searched});
            }

        }

        return model;
    }




    public Hashtable<Person, ArrayList<Account>> getHashTable(){

        return this.table;
    }

}





